package com.epam.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ConvertToXml {
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder documentBuilder;

    private void createDOMBuilder(){
        builderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = builderFactory.newDocumentBuilder();
        }catch (ParserConfigurationException ex){
            ex.printStackTrace();
        }
    }

    public void convertIntoXML(List<Bank> banks) throws TransformerException, IOException {
        createDOMBuilder();
        Document doc = documentBuilder.newDocument();

        Element titleElement=doc.createElement("banks");
        doc.createElement("banks");

        for(Bank bank:banks) {

            Element bankNode = doc.createElement("bank");

            Element nameBank = doc.createElement("name");
            nameBank.appendChild(doc.createTextNode(bank.getName()));
            bankNode.appendChild(nameBank);

            Element country = doc.createElement("country");
            country.appendChild(doc.createTextNode(bank.getRegistrationCountry()));
            bankNode.appendChild(country);

            Element depositor = doc.createElement("depositor");
            depositor.appendChild(doc.createTextNode(bank.getDepositorName()));
            bankNode.appendChild(depositor);

            Element account_id = doc.createElement("id");
            account_id.appendChild(doc.createTextNode(String.valueOf(bank.getId())));
            bankNode.appendChild(account_id);

            Element depositType = doc.createElement("type");
            depositType.appendChild(doc.createTextNode(String.valueOf(bank.getType())));
            bankNode.appendChild(depositType);

            Element amount_on_deposit = doc.createElement("amount");
            amount_on_deposit.appendChild(doc.createTextNode(String.valueOf(bank.getAmount())));
            bankNode.appendChild(amount_on_deposit);

            Element profitability = doc.createElement("profitability");
            profitability.appendChild(doc.createTextNode(String.valueOf(bank.getProbability())));
            bankNode.appendChild(profitability);

            Element time_constraints = doc.createElement("time");
            time_constraints.appendChild(doc.createTextNode(String.valueOf(bank.getTime())));
            bankNode.appendChild(time_constraints);

            titleElement.appendChild(bankNode);
        }

        doc.appendChild(titleElement);

        Transformer t = TransformerFactory.newInstance().newTransformer();
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(doc), new StreamResult(new FileOutputStream("src/main/resources/xml/newBanks.xml")));

    }
}
