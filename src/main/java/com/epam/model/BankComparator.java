package com.epam.model;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {
    public int compare(Bank o1, Bank o2) {
        if(o1.getAmount()>o2.getAmount())
            return 1;
        else if(o1.getAmount()==o2.getAmount()){
            if(o1.getAmount()>o2.getAmount()){
                return 1;
            }
            return 0;
        }
        else {
            return -1;
        }
    }
}

