package com.epam.model.parser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import java.io.File;

public class DomParser {

    private String fileXmlPath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks";
    private String fileXsdPath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks.xsd";
    private Schema schema = null;

    public void startDomParser() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File(fileXmlPath));
            document.getDocumentElement().normalize();
            Element root = document.getDocumentElement();
            System.out.println(root.getNodeName());
            NodeList nList = document.getElementsByTagName("bank");
            System.out.println("==========DOM=========");
            for (int tmp = 0; tmp < nList.getLength(); tmp++) {
                Node node = nList.item(tmp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    System.out.print("Bank name: " + eElement.getElementsByTagName("name").item(0).getTextContent());
                    System.out.print(", registration: " + eElement.getElementsByTagName("registrationCountry").item(0).getTextContent());
                    System.out.print(", depositor: " + eElement.getElementsByTagName("DepositorName").item(0).getTextContent());
                    System.out.print(" Id: " + eElement.getElementsByTagName("id").item(0).getTextContent());
                    System.out.print(", type: " + eElement.getElementsByTagName("Type").item(0).getTextContent());
                    System.out.print(", amount: " + eElement.getElementsByTagName("Amount").item(0).getTextContent() + "$");
                    System.out.print(", probability: " + eElement.getElementsByTagName("Probability").item(0).getTextContent());
                    System.out.print(", time: " + eElement.getElementsByTagName("TimeConstraints").item(0).getTextContent() + " years");
                    System.out.println();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
