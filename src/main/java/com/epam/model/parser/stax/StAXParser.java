package com.epam.model.parser.stax;

import com.epam.model.Bank;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.*;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StAXParser {

    public void startStaxParser() throws Exception {
        String xmlFilePath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks";
        File file = new File(xmlFilePath);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(file));
        List<Bank> bankList = new ArrayList<>();
        Bank bank = null;
        while (eventReader.hasNext()) {
            XMLEvent xmlEvent = eventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                if ("bank".equalsIgnoreCase(startElement.getName().getLocalPart())) {
                    bank = new Bank();
                }
                Iterator<Attribute> iterator = startElement.getAttributes();

                while (iterator.hasNext()) {
                    Attribute attribute = iterator.next();
                    QName name = attribute.getName();
                    if ("name".equalsIgnoreCase(name.getLocalPart())) {
                        bank.setName(attribute.getValue());
                    }
                }
                switch (startElement.getName().getLocalPart()){
                    case "name":
                        Characters nameDataEvent = (Characters) eventReader.nextEvent();
                        bank.setName(nameDataEvent.getData());
                        break;
                    case "registrationCountry":
                        Characters countryDataEvent = (Characters) eventReader.nextEvent();
                        bank.setRegistrationCountry(countryDataEvent.getData());
                        break;
                    case "DepositorName":
                        Characters depositorNameDataEvent = (Characters) eventReader.nextEvent();
                        bank.setDepositorName(depositorNameDataEvent.getData());
                        break;
                    case "id":
                        Characters idDataEvent = (Characters) eventReader.nextEvent();
                        bank.setId(Integer.parseInt(idDataEvent.getData()));
                        break;
                    case "Type":
                        Characters typeDataEvent = (Characters) eventReader.nextEvent();
                        bank.setType(typeDataEvent.getData());
                        break;
                    case "Amount":
                        Characters amountDataEvent = (Characters) eventReader.nextEvent();
                        bank.setAmount(Integer.parseInt(amountDataEvent.getData()));
                        break;
                    case "Probability":
                        Characters probabilityDataEvent = (Characters) eventReader.nextEvent();
                        bank.setProbability(Double.parseDouble(probabilityDataEvent.getData()));
                        break;
                    case "TimeConstraints":
                        Characters timeDataEvent = (Characters) eventReader.nextEvent();
                        bank.setTime(Integer.parseInt(timeDataEvent.getData()));
                        break;
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if ("bank".equalsIgnoreCase(endElement.getName().getLocalPart())) {
                    bankList.add(bank);
                }
            }
        }
        System.out.println("=======StAX======");
        System.out.println(bankList);
    }
}