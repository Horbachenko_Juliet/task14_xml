package com.epam.model.parser.sax;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.model.Bank;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class SaxParser {

    private String fileXmlPath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task14_xml\\src\\main\\resources\\xml\\banks";

    public List parseXml(InputStream in) {
        List<Bank> banks = new ArrayList<>();
        try {
            SaxHandler handler = new SaxHandler();
            XMLReader parser = XMLReaderFactory.createXMLReader();
            parser.setContentHandler(handler);
            InputSource source = new InputSource(in);
            parser.parse(source);
            banks = handler.getUsers();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        } return banks;
    }

    public void startSaxParser() throws FileNotFoundException {
        File xmlFile = new File(fileXmlPath);
        List banks = parseXml(new FileInputStream(xmlFile));
        System.out.println(banks);
    }
}
